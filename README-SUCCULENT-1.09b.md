## Succulent
##### By: Jonathan Vasquez (fearedbliss)
##### Build: 2021-03-01-1800

## Description

A **`Diablo II: 1.09b`** modification tailored for Single Player. Succulent
contains strong influence from **`Diablo I`**, and **`Diablo II (1.00 - 1.05b)`**.

## Quality of Life

- The game now has an Expanded Stash and Cube for both Classic and Expansion.
- The game now has an Infinite Stash for both Classic and Expansion. **`(Launch Game w/ Alpaca.exe)`**
- All new characters start out with the Horadric Cube.
- Fara (Act 2) and Ormus (Act 3) now sell more misc items.
- You can now open the Cow Level even if you kill the King.
- You can now open the Cow Level with just a Town Portal Book.
- Nihlathak's Portal no longer closes when you get the Halls of Pain WP and have
  already killed him.
- The Battle.net button has been disabled for safety reasons.
- The ambiguity between the number "5" and "6" has been fixed.
- You can now run multiple clients of Diablo II.
- You are now able to quickly join LAN games.
- Fixed CPU usage bug in the Main Menu, Single Player, and LAN games.
- The introduction cinematics are now automatically skipped.
- The game will now work with the MPQ files included in the new Blizzard Installer.
- The scrolling letters slowdown is now fixed for both **`DirectDraw`** and **`Glide`**.
- The FPS is now unlocked in Single Player. **`(Same as LAN games)`**
- You no longer need the CD in order to play the game.
- You can now make Hardcore characters without beating Softcore.

## Gameplay Changes

#### Pre 1.07

- General monster immunities are no longer in the game. **`(Resistances Capped @ 90%)`**
  - Enchanted monsters can still be immune.
- Gambling rates are back to pre-LOD. **`(Uniques: 3%, Sets: 5%, Rares: 7%)`**
- Rares can now spawn with any affix.

#### Drop Balancing

- Improved Item and Rune Drop Rates.
- The Countess and Chests can now drop higher Runes on each difficulty. **`(Normal: Sol, NM: Ist, Hell: Zod)`**
- Perfect Gems now have a chance to drop.

## Notes

Succulent is based off my [Singling](README-SINGLING.md) changes as a base.
Please take a look at the [Notes](README-SINGLING.md#notes) section for things
that may affect your game experience.
